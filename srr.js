var items = [
    ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpg'], 
    ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpg'],
    ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
    ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpg']
]

var listBarang = document.getElementById("listbarang")
var tampung = ""
for (var i= 0; i<items.length; i++ ){
    tampung += ` 
                <div class="card" style="width: 18rem;">
                <img src="..." class="card-img-top" height="200px" width="200px" alt="...">
                <div class="card-body">
                <h5 class="card-title" id="itemName">Nama</h5>
                <p class="card-text" id="itemDesc">Deskripsi barang</p>
                <p class="card-text">Rp Harga</p>
                <a href="#" class="btn btn-primary" id="addCart">Tambahkan ke keranjang</a>
                </div>
                </div>
                `
}
listBarang.innerHTML = tampung